# KPI Lectures
Here I will store all lectures I save on my machine. Lectures are stored as LaTeX documents.

## Contents
- `psychology-lectures.tex` --- Everything I write on psychology studies.
- `generate.sh` --- simple script, which compiles all `.tex` files into `.pdf` files.
- `clean.sh` --- script that removes output files (*.out, *.toc, *.log, *.aux).

## How to get pdf file
You will need LaTeX installed. You can compile a concrete file using `pdflatex <filename>` command. Or you can use provided script in order to compile all at once.
